# Common information

This package provides types for `@scythe/core` projects.

# Installation

`npm i @scythe/types` or `yarn add @scythe/types`

## Module

This class will help you to include modules in your application which needed to be initiated and stoped  
Usage:

```typescript
import { Module, AbstractScytheApplication } from '@scythe/types';

export class YourModule extends Module {
  public initialize<T extends AbstractScytheApplication>(app: T) {
    //initialize your module here
  }
  public stop<T extends AbstractScytheApplication>(app: T) {
    //Stop your module here
  }
}
```

Notice that `app` param in the `initialize` and `stop` method are instance of your `ScytheApplication`

And it'll run `initialize` method before application starts and `stop` on application stops
