import * as http from 'http';
import { Express, RequestHandler } from 'express';
import { Module } from './Module';
import { OpenApiBuilder } from 'openapi3-ts';

export type TJobHandler = () => any | Promise<any>;

type TOpenAPI<T extends OpenApiBuilder = OpenApiBuilder> = T extends OpenApiBuilder ? T : never;

export abstract class AbstractScytheApplication {
  public abstract server?: Express;
  public abstract httpServer?: http.Server;
  public abstract openAPI: TOpenAPI;
  public abstract addRouter(url: string, router: any): void;
  public abstract addModule<T extends Module>(module: T): void;
  public abstract async start(config: any, logPath: string, additionalMiddlewares: RequestHandler[]): Promise<void>;

  public abstract addJob(schedule: string, job: TJobHandler): void;
  public abstract async stop(): Promise<void>;
  public abstract getOpenAPIDocs(): any;
  public abstract registerController(Controller: any, basePath?: string): void;
}
