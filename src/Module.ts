/**
 * Module base class
 */
import { AbstractScytheApplication } from './ScytheApplication';

export class Module {
  /**
   * Initializes the module
   * @param {Application} app application instance
   */
  public initialize<T extends AbstractScytheApplication>(app: T) {}

  /**
   * Stops the module
   * @param {Application} app application instance
   */
  public stop<T extends AbstractScytheApplication>(app: T) {}
}
